Spring2016-Xueting Yu-447920

Creative Point:
1. User can see current date and time after login
2. User can send private message to any online user if staying on public room
3. User can send same private message to multiple users in the same room, separate names by comma
4. User can send picture through URL in the chat room
5. User can see (send out time) when does each message send out
6. When the admin (the creater) of a chat room leave, people who enter the room earliest will become the admin, get the authority to kick out user and ban user
7. Applied bootstrap framework for UI design

url: http://ec2-54-68-77-79.us-west-2.compute.amazonaws.com:3456/clientcp.html#